package com.heima;

import com.heima.utils.common.BCrypt;
import org.springframework.util.DigestUtils;

public class PasswordTest {
    public static void main(String[] args) {
          //md5加密  DegestUtils：spring框架提供的工具类
        String md5Str = DigestUtils.md5DigestAsHex("abc".getBytes());
        System.out.println(md5Str);//900150983cd24fb0d6963f7d28e17f72*/

        //  password:123   salt:随时字符串
      /*  String salt = RandomStringUtils.randomAlphanumeric(10);
        String pswd = "123"+salt;
        System.out.println(pswd);

        String saltPswd = DigestUtils.md5DigestAsHex(pswd.getBytes());
        System.out.println(saltPswd);*/

        //password=admin,salt=123abc
        /*String ps="admin"+"123abc";
        String s = DigestUtils.md5DigestAsHex(ps.getBytes());
        System.out.println(s);*/

//Bcrypt进行密码加密
        String gensalt = BCrypt.gensalt();//这个是盐  29个字符，随机生成
        System.out.println(gensalt);

        String password = BCrypt.hashpw("123456", gensalt);  //根据盐对密码进行加密
        System.out.println(password);//加密后的字符串前29位就是盐

        //校验
        boolean checkpw = BCrypt.checkpw("123456", password);
        System.out.println(checkpw);

    }
    }
